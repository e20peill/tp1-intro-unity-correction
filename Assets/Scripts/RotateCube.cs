﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RotateCube : MonoBehaviour
{

    public float speed = 3.0f;
    public InputAction activeRotation;

    private bool rot = false;

    // Start is called before the first frame update
    void Start()
    {
        activeRotation.Enable();
        activeRotation.performed += ctx => rot = !rot;
    }

    // Update is called once per frame
    void Update()
    {
        if (rot)
        {
            transform.Rotate(0, speed * Time.deltaTime, 0);
        }
    }
}
