﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CreateOnClick : MonoBehaviour
{

    public GameObject aCopier;
    public InputAction receiveRightClick;

    // Start is called before the first frame update
    void Start()
    {
        receiveRightClick.Enable();
        receiveRightClick.performed += ctx =>
        {
            GameObject obj = Instantiate(aCopier);
            obj.GetComponent<Renderer>().material.color = Color.yellow;
            obj.transform.position = transform.position + transform.forward * 2;
            obj.GetComponent<Rigidbody>()
                  .AddForce(transform.forward * 1000);
        };
    }

}
